# Feel free to change / extend / adapt this source code as needed to complete the homework, based on its requirements.
# This code is given as a starting point.
#
# REFERENCES
# The code is partly adapted from pytorch tutorials, including https://pytorch.org/tutorials/beginner/blitz/cifar10_tutorial.html

# ---- hyper-parameters ----
# You should tune these hyper-parameters using:
# (i) your reasoning and observations, 
# (ii) by tuning it on the validation set, using the techniques discussed in class.
# You definitely can add more hyper-parameters here.

import argparse

ap = argparse.ArgumentParser()
ap.add_argument("--mode",required=True, help="Mode of the code (test, validation or train) ")
args = vars(ap.parse_args())


batch_size = 16
max_num_epoch = 100
stop_sublimit = 0.1  # when loss increase that much, stop the training

TRAIN = "train"
VALIDATION = "validation"
TEST = "test"
mode = args["mode"].lower()

hps = {
    'lr':0.1, # learning rate
    'ks':3, # Kernel size
    'ln':4, # Number of convolutional layers
    'kn':16,  # Number of kernels in each layer
}

import torch
# ---- options ----
DEVICE_ID = "cuda" if torch.cuda.is_available() else "cpu" # set to 'cpu' for cpu, 'cuda' / 'cuda:0' or similar for gpu.
LOG_DIR = '../checkpoints'
VISUALIZE = False # set True to visualize input, prediction and the output from the last batch
LOAD_CHKPT = False if mode in [VALIDATION, TEST] else False

# --- imports ---
import os
import sys
import matplotlib.pyplot as plt
import numpy as np
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torchvision.transforms as transforms
from torchsummary import summary
import math
import time

import hw3utils

torch.multiprocessing.set_start_method('spawn', force=True)

# ---- utility functions -----
def get_loaders(batch_size,device):
    data_root = '../dataset' 

    train_set = hw3utils.HW3ImageFolder(root=os.path.join(data_root,'train'),device=device)
    train_loader = torch.utils.data.DataLoader(train_set, batch_size=batch_size, shuffle=True, num_workers=0)
    val_set = hw3utils.HW3ImageFolder(root=os.path.join(data_root,'val'),device=device)
    val_loader = torch.utils.data.DataLoader(val_set, batch_size=batch_size, shuffle=False, num_workers=0)
    return train_loader, val_loader
        

# ---- ConvNet -----
class Net(nn.Module):
    def __init__(self, kernel_size, layer_number, kernel_number):
        super(Net, self).__init__()
        self.layer_number = layer_number # Number of layers
        self.kernel_number = kernel_number # Number of kernels in one layer
        self.kernel_size = kernel_size # Kernel size
        self.input_channel = 1 # Grayscale input
        self.output_channel = 3 # RGB output

        if(self.layer_number == 1):
            self.input_layer = nn.Sequential(
                nn.Conv2d(self.input_channel, self.output_channel,
                    self.kernel_size, padding=(kernel_size-1) // 2))
        else:
            self.input_layer = nn.Sequential(
                nn.Conv2d(self.input_channel , self.kernel_number, 
                    self.kernel_size, padding=(kernel_size-1) // 2),
                nn.ReLU(inplace=True))

        self.hidden_layer = nn.Sequential(
            nn.Conv2d(self.kernel_number, self.kernel_number,
                self.kernel_size, padding=(kernel_size-1) // 2),
            nn.ReLU(inplace=True))

        self.output_layer = nn.Sequential(
            nn.Conv2d(self.kernel_number,self.output_channel,
                self.kernel_size, padding=(kernel_size-1) // 2))

    def forward(self, grayscale_image):
        # apply your network's layers in the following lines:

        # If there is only 1 layer
        if(self.layer_number == 1):
            x = self.input_layer(grayscale_image) # Input and ouput layer
        else :
            x = self.input_layer(grayscale_image) # Input layer
            
            for i in range(1, self.layer_number - 1):
                x = self.hidden_layer(x)

            x = self.output_layer(x)

        return x

# ---- initialization -----
#torch.manual_seed(0) # Set seed to 0
device = torch.device(DEVICE_ID)
print('device: ' + str(device))
model = Net(kernel_size = hps["ks"], layer_number = hps["ln"], kernel_number = hps["kn"]).to(device=device)

criterion = nn.MSELoss()
optimizer = optim.SGD(model.parameters(), lr=hps['lr'])

if(mode == TRAIN):
    train_loader, val_loader = get_loaders(batch_size,device)
elif(mode == TEST):
     test_loader = get_loaders(batch_size,device)

train_losses, val_losses, val_accuracies = [], [], []

if LOAD_CHKPT:
    print('loading the model from the checkpoint')
    model.load_state_dict(torch.load(os.path.join(LOG_DIR,'checkpoint.pt')))

def train():
    model.train()
    train_loss = 0.0 # training loss of the network
    for step, (data, target) in enumerate(train_loader, 0):
        data, target = data.to(device), target.to(device)
        optimizer.zero_grad() # zero the parameter gradients

        # do forward, backward, SGD step
        pred = model(data)
        loss = criterion(pred, target)
        loss.backward()

        optimizer.step()
        train_loss += loss.item()

    train_loss /= len(train_loader)

    if (step == 0) and VISUALIZE: 
            hw3utils.visualize_batch(data, pred, target)

    return train_loss

# Evaluate performance on validation set
def evaluate():
    model.eval()
    val_loss = 0.0
    val_acc = 0.0
    with torch.no_grad():
        for data , target in val_loader:
            data, target = data.to(device), target.to(device)
            pred = model(data)         
            loss = criterion(pred, target)
            val_loss += loss.item()
            val_acc += find_acc(pred, target)

    val_acc /= len(val_loader)
    val_loss /= len(val_loader)

    return val_loss, val_acc

# Make inference on the test set
def test(test_directory):
    data_root = '../dataset' 
    test_set = hw3utils.HW3ImageFolder(root=os.path.join(data_root,test_directory),device=device)
    test_loader = torch.utils.data.DataLoader(test_set, batch_size=1, shuffle=False, num_workers=0)

    estimations = np.empty( shape=(len(test_loader), 3, 80, 80) )

    with torch.no_grad():
        for step, (data, target) in enumerate(test_loader) :

            data, target = data.to(device), target.to(device)

            pred = model(data).view(3, 80, 80).cpu().numpy() if DEVICE_ID == "cuda" else model(data).view(3, 80, 80).numpy()
            target = target.view(3, 80, 80).cpu().numpy() if DEVICE_ID == "cuda" else target.view(3, 80, 80).numpy()

            pred , target = (pred/2+0.5)*255, (target/2+0.5)*255
            pred , target = pred.astype('uint8') , target.astype('uint8')
            """
            predicted_image = transforms.functional.to_pil_image(pred/2+0.5)
            groundtruth_image = transforms.functional.to_pil_image(target/2+0.5)

            predicted_image.save("prediction{}.png".format(step + 1))
            groundtruth_image.save("groundtruth{}.png".format(step + 1))
            """
            estimations[step] = pred

    return estimations

def save_loss_plot(): 
    plt.plot(train_losses, label='Training loss')
    plt.legend(frameon=False)
    plt.savefig(os.path.join(LOG_DIR,'loss.png'))
    plt.clf()

def save_accuracy_plot(): 
    plt.plot(val_accuracies, label='Validation Accuracy')
    plt.legend(frameon=False)
    plt.savefig(os.path.join(LOG_DIR,'accuracy.png'))
    plt.clf()

def save_error_plot():
    val_errors = 1.0 - np.array(val_accuracies)
    plt.plot(val_errors, label="Error of margin 12")
    plt.legend(frameon=False)
    plt.savefig(os.path.join(LOG_DIR,'error.png'))
    plt.clf()

# Accepts as correct in the margin of error 12
def find_acc(pred, target):
    unnormalized_target = (((target/2.0)+0.5)*255).clone()
    unnormalized_pred = (((pred/2.0)+0.5)*255).clone()
    difference_tensor = torch.abs(torch.sub(unnormalized_target,unnormalized_pred))
    mask_tensor = torch.where(difference_tensor < 12, 
                torch.tensor([1.], device=device),
                torch.tensor([0.], device=device))
    mask_tensor = mask_tensor.reshape(1,-1).squeeze()
    acc = torch.sum(mask_tensor) / mask_tensor.shape[0]

    return acc.item()

def main():

    if(mode == TRAIN) :
        if not os.path.exists(LOG_DIR):
            os.makedirs(LOG_DIR)

        min_val_acc = 0.0
        for epoch in range(0, max_num_epoch):
            currTime = time.time()

            train_loss = train()
            val_loss, val_acc = evaluate()

            train_losses.append(train_loss)
            val_losses.append(val_loss)
            val_accuracies.append(val_acc)

            print('Epoch : %d, train-loss: %.3f val-loss: %.3f val-acc: %.3f' %
                    (epoch + 1, train_loss, val_loss, val_acc))
        
            if(val_acc > min_val_acc):
                min_val_acc = val_acc

            # Stop the training
            elif(epoch > 10 and val_acc < min_val_acc - stop_sublimit):
                print("Early stopping the training")
                break
            
            elapsedTime = (time.time() - currTime)

            print('Saving the model end of epoch %d in %d sec' % (epoch+1, elapsedTime))
            torch.save(model.state_dict(), os.path.join(LOG_DIR,'checkpoint.pt'))

        print('Finished Training')
        save_loss_plot()
        save_accuracy_plot()
        save_error_plot()
        print("Saved the plottings")

        with open("results.out","a+") as result_file:
            result_file.write("(batch size: {}, lr: {} , ks: {} , ln: {} , kn : {}) -> (v-loss: {}, v-acc: {}) \n"
                            .format(
                            str(batch_size),
                            str(hps["lr"]),
                            str(hps["ks"]),
                            str(hps["ln"]),
                            str(hps["kn"]),
                            str(val_loss),
                            str(val_acc)))

    elif(mode == VALIDATION):
        estimations = test("val")
        np.save("estimations_validation.npy", estimations)
        
    elif(mode == TEST):
        estimations = test("test")
        np.save("estimations_test.npy", estimations)
    

if __name__ == "__main__" :
    main()


